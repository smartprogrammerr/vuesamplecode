﻿import { RouteConfig } from 'vue-router';
const Error403Page = () => import(/* webpackChunkName: "403-page" */'../views/403.vue');

const routes: RouteConfig[] = [
    {
        path: "error/403",
        name: "error.403",
        component: Error403Page,
    },
];

export default routes;
