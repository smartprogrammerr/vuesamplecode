﻿import { RouteConfig } from 'vue-router';
import appRoutes from './app.route';
import anonymousRotes from './anonymous.route';
const AppLayout = () => import(/* webpackChunkName: "app-layout" */'../layout/app.layout.vue');
const AnonymousLayout = () => import(/* webpackChunkName: "anonymous-layout" */'../layout/anonymous.layout.vue');

const routes: RouteConfig[] = [
    {
        path: '',
        component: AppLayout,
        children: appRoutes,
    },
    {
        path: '',
        component: AnonymousLayout,
        children: anonymousRotes
    },
];

export default routes;
