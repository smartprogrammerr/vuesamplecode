import Vue from 'vue';
import Router, { RouteConfig } from 'vue-router';
import Routes from './routes';
import { CheckRequest } from '../helper/request.check';
Vue.use(Router);
const routeConfig: RouteConfig[] = Routes;

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: routeConfig,
});

router.beforeEach(async (to, from, next) => {
    const isWhite = CheckRequest.isRouterWhiteRequest(to.name);
    if (isWhite) {
        next();
        return;
    }
    if (await CheckRequest.routeHasRole(to.meta.roles)) {
        next();
        return;
    }
    else {
        next('/error/403');
    }
});


export default router;