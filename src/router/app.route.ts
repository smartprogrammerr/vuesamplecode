﻿import { RouteConfig } from "vue-router";
import Picker from "../views/Picker.vue";

const Home = () =>
    import(/* webpackChunkName: "home-page" */ "../views/Home.vue");
const FetchData = () =>
    import(/* webpackChunkName: "fetch-data" */ "../views/FetchData.vue");
const Forbidden = () =>
    import(/* webpackChunkName: "forbidden" */ "../views/Forbidden.vue");
const About = () =>
    import(/* webpackChunkName: "about-page" */ "../views/About.vue");
const AddStaffProfile = () =>
    import(/* webpackChunkName: "about-page" */ "../views/staff/add.vue");
const ListStaffProfile = () =>
    import(/* webpackChunkName: "about-page" */ "../views/staff/list.vue");

const AddClientTrackingItem = () =>
    import("../views/client.tracking/add.vue");
const ListClientTrackingItem = () =>
    import("../views/client.tracking/list.vue");


    const ListUnArchivedClientTrackingItem = () =>
    import("../views/client.tracking/archive.list.vue");

const AddDueDateExtension = () =>
    import(
    /* webpackChunkName: "add-duedate-extension-page" */ "../views/duedate.extension/add.vue"
    );
const ListDueDateExtension = () =>
    import(
    /* webpackChunkName: "list-duedate-extension-page" */ "../views/duedate.extension/list.vue"
    );
const EmptyLayout = () => import("../layout/empty.layout.vue");


const routes: RouteConfig[] = [    {
        path: "",
        name: "Home",
        component: Home,
        meta: {
            roles: ['Administrator','OfficeAdmin','User'],
        },
    },
    {
        path: "staffprofile",
        name: "app.staffprofile",
        component: EmptyLayout,
        meta: {
            roles: ['Administrator','OfficeAdmin'],
        },
        children: [
            {
                path: "",
                name: "app.staffprofile.list",
                component: ListStaffProfile,
                meta: {
                    roles: ['Administrator','OfficeAdmin'],
                },
            },
            {
                path: "add",
                name: "app.staffprofile.add",
                component: AddStaffProfile,
                meta: {
                     roles: ['Administrator','OfficeAdmin'],
                },
            },
            {
                path: ":id/edit",
                name: "app.staffprofile.edit",
                component: AddStaffProfile,
                meta: {
                     roles: ['Administrator','OfficeAdmin'],
                },
            },
        ],
    },
    {
        path: "clienttracking",
        name: "app.clienttracking",
        component: EmptyLayout,
        meta: {
            roles: ['Administrator','OfficeAdmin','User'],
        },
        children: [
            {
                path: "clientdeadline",
                name: "app.clienttracking.clientdeadline",
                component: ListClientTrackingItem,
                alias: '',
                meta: {
                    roles: ['Administrator','OfficeAdmin','User'],
                },
            },
            {
                path: "alldata",
                name: "app.clienttracking.alldata",
                component: ListClientTrackingItem,
                meta: {
                    roles: ['Administrator','OfficeAdmin','User'],
                },
            },
            {
                path: "personal",
                name: "app.clienttracking.personal",
                component: ListClientTrackingItem,
                meta: {
                    roles: ['Administrator', 'OfficeAdmin', 'User'],
                },
            },
            {
                path: "add",
                name: "app.clienttracking.add",
                component: AddClientTrackingItem,
                meta: {
                     roles: ['Administrator','OfficeAdmin'],
                },
            },
            {
                path: ":id/edit",
                name: "app.clienttracking.edit",
                component: AddClientTrackingItem,
                meta: {
                     roles: ['Administrator','OfficeAdmin'],
                },
            },
            {
                path: ":id/view",
                name: "app.clienttracking.view",
                component: AddClientTrackingItem,
                meta: {
                       roles: ['Administrator','OfficeAdmin','User'],
                },
            },
            {
                path: ":id/copy",
                name: "app.clienttracking.copy",
                component: AddClientTrackingItem,
                meta: {
                       roles: ['Administrator','OfficeAdmin'],
                },
            },
            {
                path: ":id/additionalreturn",
                name: "app.clienttracking.additionalreturn",
                component: AddClientTrackingItem,
                meta: {
                       roles: ['Administrator','OfficeAdmin'],
                },
            },
            {
                path: ":id/child/add",
                name: "app.clienttracking.child.add",
                component: AddClientTrackingItem,
                meta: {
                    roles: ['Administrator', 'OfficeAdmin'],
                },
            },
            {
                path: ":headClientId/childs",
                name: "app.clienttracking.childs",
                component: ListClientTrackingItem,
                meta: {
                    roles: ['Administrator', 'OfficeAdmin'],
                },
            },
            {
                path: "archive",
                name: "app.clienttracking.archive",
                component: ListUnArchivedClientTrackingItem,
                meta: {
                       roles: ['Administrator','OfficeAdmin'],
                },
            },
        ],
    },
 
   
    {
        path: "duedate",
        name: "duedate.extension",
        component: EmptyLayout,
        meta: {
            roles: ['Administrator','OfficeAdmin','User'],
        },
        children: [
            {
                path: "",
                name: "app.duedate.extension.list",
                component: ListDueDateExtension,
                meta: {
                    roles: ['Administrator','OfficeAdmin','User'],
                },
            },
            {
                path: ":id/edit",
                name: "app.duedate.extension.edit",
                component: AddDueDateExtension,
                meta: {
                       roles: ['Administrator','OfficeAdmin'],
                },
            },
            {
                path: "add",
                name: "app.duedate.extension.add",
                component: AddDueDateExtension,
                meta: {
                       roles: ['Administrator','OfficeAdmin'],
                },
            },
        ],
    }
];

export default routes;
