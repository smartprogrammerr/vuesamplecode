
import 'core-js/stable';
import 'regenerator-runtime/runtime'; 

import Vue from 'vue'
import App from './App.vue'
import router from '@/router/index'
import store from '@/store/index';
import vuetify from './plugins/vuetify';
import './registerServiceWorker';
import './plugins/kendo';
import './plugins/vue.injector';
import './plugins/vee-validate';
import './router/componentHooks';
import './plugins/browser.detect';
import './plugins/jquery';
import './assets/js/floatingscrollbar.js';
import PeoplePicker from "@/components/PeoplePicker.vue";
import MultiPeoplePicker from "@/components/MultiPeoplePicker.vue";
import { ValidationProvider, extend } from 'vee-validate';
import CustomGridComponent from './components/common/custom.grid.vue';

Vue.config.productionTip = false;
Vue.component('People-Picker', PeoplePicker);
Vue.component('Multi-People-Picker', MultiPeoplePicker);
Vue.component('custom-grid', CustomGridComponent);
Vue.component('ValidationProvider', ValidationProvider);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
