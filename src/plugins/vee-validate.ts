﻿import Vue from "vue";
import { required, email, max, numeric, integer, } from 'vee-validate/dist/rules';
import { extend, ValidationProvider, ValidationObserver, setInteractionMode } from 'vee-validate';

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
setInteractionMode('eager');

extend('required', {
    ...required,
    message: '{_field_} is required',
})

extend('max', {
    ...max,
    message: '{_field_} may not be greater than {length} characters',
})

extend('email', {
    ...email,
    message: 'Email must be valid',
})

extend('number', {
    ...numeric,
    message: 'Please enter only numeric value',
})

extend('integer', {
    ...integer,
    message: 'Please enter only numeric value',
})

const doubleRule = {
    message: 'Please enter only numeric value',
    validate(value, args) {
        const doubleRegx = /^-?[0-9]+([.][0-9]+)?$/;
        return doubleRegx.test(value);
    }
};
extend('double', doubleRule);

const phoneRule = {
    getMessage(field, args) {
        return `The ${field} must be a valid phone`;
    },
    validate(value, args) {
        const MOBILEREG = /^((1[3578][0-9])+\d{8})$/;
        return MOBILEREG.test(value);
    }
};
extend('phone', phoneRule);