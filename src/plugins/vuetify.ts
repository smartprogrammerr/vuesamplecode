import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);
export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: "#e0301e",
                secondary: colors.grey.darken4,
                accent: colors.shades.black,
                error: colors.red.accent3,
            },
        },
    },
});
