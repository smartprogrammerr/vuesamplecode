﻿import Vue from 'vue';
import injector from 'vue-inject';

Vue.use(injector);