import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { Utils } from "../helper/utils";
import { ExceptionHandler } from "../models/AppEvent";

export const client = axios.create({
    baseURL: Utils.API_BASE_URL,
});

client.interceptors.request.use(
    (config: AxiosRequestConfig) => {
        const isProtectedHeader = config.headers["isProtected"];
        if (isProtectedHeader && isProtectedHeader !== undefined) {
            delete config.headers["isProtected"];
        } else {
            if (isProtectedHeader !== undefined) {
                delete config.headers["isProtected"];
            }
        }
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

client.interceptors.response.use(
    (response: AxiosResponse<any>) => {
        return response;
    },
    function (error) {
        if (error) {
            if (error.response && error.response.data) {
                let statusCode = error.response.data.StatusCode;
                if (!statusCode) {
                    statusCode = error.response.data.status;
                }
                switch (statusCode) {
                    case 400: {
                        ExceptionHandler.open(
                            "Invalid request",
                            "The request is not valid. Please fill all the required fields."
                        );
                        break;
                    }
                    case 401:
                    case 403: {
                        ExceptionHandler.open(
                            "Unauthorized",
                            "You are not authorized to View this page"
                        );
                        break;
                    }
                    case 404: {
                        ExceptionHandler.open(
                            "Not Found",
                            "The resource you are looking for does not exist"
                        );
                        break;
                    }
                    case 500: {
                        ExceptionHandler.open(
                            "Error",
                            "Something went wrong. Please try again."
                        );
                        break;
                    }
                    case 302:
                    default: {
                        ExceptionHandler.open(
                            "Session Expired",
                            "Your session has expired. Please refresh and login again"
                        );
                        break;
                    }
                }
            }
            else {
                if (error.response === undefined) {
                    ExceptionHandler.open(
                        "Session Expired",
                        "Your session has expired. Please refresh and login again"
                    );
                }
            }
        }

        return Promise.reject(error);
    }
);
