﻿import store from '../store/index';
import { Injector } from '../services/injector';

export class CheckRequest {

    public static async routeHasRole(roles: string[]): Promise<boolean> {

        let userroles = store.getters.getRoles;
       
        if (!userroles || !userroles.length) {
            const userInfo = await Injector.UserService.getRoles();           
            userroles = userInfo.roles;
            store.dispatch('setUserInfo', userInfo);

        }
        if (userroles && userroles.length && roles && roles.length) {
            for (let i = 0; i < roles.length; i++) {
                const index = userroles.indexOf(roles[i]);
                if (index > -1) {
                    return true;
                }
            }
        }
        return false;
    }

    public static isApiWhiteRequest(request: any): boolean {
        const requestUrl = new URL(request);

        const isWhiteRequest = this.apiWhiteList.findIndex(
            (item: string) => {
                const requestPathName = requestUrl.pathname;
                return requestPathName.indexOf(item) > -1;
            }) > -1;

        return isWhiteRequest;
    }

    public static isRouterWhiteRequest(router: string | null | undefined): boolean {
        const isWhiteRequest = this.routerWhiteList.findIndex(
            (item: string) => {
                return item === router;
            },
        ) > -1;
        return isWhiteRequest;
    }

    private static apiWhiteList: string[] = [

    ];

    private static routerWhiteList: string[] = [
        'error.403'
    ];
}
