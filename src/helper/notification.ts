﻿import toastr from 'toastr';

export abstract class Notification {

    public static show(status: boolean, message: string, timeOut?: number) {
        this.notify({
            iconClass: status ? 'success' : 'error',
            title: status ? 'Success' : 'Error',
            type: status ? 'success' : 'error',
            timeOut,
            message,
        });
    }

    private static notify(options: any) {
        toastr.options = {
            closeButton: true,
            debug: false,
            newestOnTop: false,
            progressBar: false,
            positionClass: 'toast-top-right',
            preventDuplicates: false,
            showDuration: 300,
            hideDuration: 1000,
            timeOut: 5000,
            extendedTimeOut: 1000,
            showEasing: 'swing',
            hideEasing: 'linear',
            showMethod: 'fadeIn',
            hideMethod: 'fadeOut',
            tapToDismiss: false,
        };
        if (options.timeOut) {
            toastr.options.timeOut = options.timeOut;
            toastr.options.extendedTimeOut = options.timeOut;
        }
        switch (options.type) {
            case 'success': {
                toastr.success(options.message, options.title);
                break;
            }
            case 'error': {
                toastr.error(options.message, options.title);
                break;
            }
            case 'warning': {
                toastr.warning(options.message, options.title);
                break;
            }
        }
    }
}
