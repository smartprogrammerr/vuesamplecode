﻿export abstract class Utils {
    public static API_BASE_URL = "http://localhost:5000/api"; //process.env.VUE_APP_USCORP_API_BASE_URL;

  public static isEquivalent(obj: any, obj1: any): boolean {
    obj = JSON.parse(JSON.stringify(obj));
    obj1 = JSON.parse(JSON.stringify(obj1));

    // Create arrays of property names
    const aProps = Object.getOwnPropertyNames(obj);
    const bProps = Object.getOwnPropertyNames(obj1);

    // If number of properties is different,
    // objects are not equivalent
    if (aProps.length !== bProps.length) {
      return false;
    }

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < aProps.length; i++) {
      const propName = aProps[i];

      // If values of same property are not equal,
      // objects are not equivalent
      if (obj[propName] !== obj1[propName]) {
        return false;
      }
    }

    // If we made it this far, objects
    // are considered equivalent
    return true;
  }

  public static copy(obj: any): any {
    return JSON.parse(JSON.stringify(obj));
  }

  public static objectToQueryString(obj: any) {
    return Object.keys(obj)
      .map(function(key) {
        return key + "=" + obj[key];
      })
      .join("&");
  }

  public static DownloadCSVForIE(tdData, fileName) {
    const blob = new Blob([tdData], { type: "text/csv" });
    window.navigator.msSaveOrOpenBlob(blob, fileName + ".csv");
  }

  public static base64ToArrayBuffer(base64) {
    const binaryString = window.atob(base64);
    const binaryLen = binaryString.length;
    const bytes = new Uint8Array(binaryLen);
    for (let i = 0; i < binaryLen; i++) {
      const ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }
  public static downloadBytes(byte, fileExtension, filename) {
    const blob = new Blob([byte]);
    const link = document.createElement("a");
    link.href = window.URL.createObjectURL(blob);
    const fileName = filename + fileExtension;
    link.download = fileName;
    link.click();
  }
}
