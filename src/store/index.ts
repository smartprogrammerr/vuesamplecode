import Vue from 'vue'
import Vuex, { ActionContext } from 'vuex';
import { UserInfo } from '../models/userInfo';

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        pageTitle: 'AppName',
        userInfo: {} as UserInfo,
        office: '',
        offices: [],
        systemConfig: null as any
    },
    mutations: {
        setPageTitle: (state, appname) => {
            state.pageTitle = appname;
        },
        setUserInfo: (state, userInfo) => {
            state.userInfo = userInfo;
        },
        setOfficeValue: (state, office) => {
            state.office = office;
        },
        setOffices: (state, offices) => {
            state.offices = offices;
        },
        setSystemConfig: (state, systemConfig) => {
            state.systemConfig = systemConfig;
        },
    },
    actions: {
        setPageTitle: (context: ActionContext<any, any>, title) => {
            context.commit('setPageTitle', title);
        },
        setUserInfo: (context: ActionContext<any, any>, userInfo) => {
            context.commit('setUserInfo', userInfo);
        },
        setOfficeValue: (context: ActionContext<any, any>, office) => {
            localStorage.removeItem("officeSelected");
            localStorage.setItem("officeSelected", office as any);
            context.commit('setOfficeValue', office);
        },
        setOffices: (context: ActionContext<any, any>, offices) => {
            context.commit('setOffices', offices);
        },
        setSystemConfig: (context: ActionContext<any, any>, systemConfig) => {
            context.commit('setSystemConfig', systemConfig);
        },
    },
    getters: {
        getTitle: (state, getters) => {
            return state.pageTitle;
        },
        getRoles: (state, getters) => {
            return state.userInfo.roles;
        },
        getEmail: (state, getters) => {
            return state.userInfo.email;
        },
        getOrganizationCode: (state, getters) => {
            return state.userInfo.organizationCode;
        },
        getPhone: (state, getters) => {
            return state.userInfo.phone;
        },
        getStaffCode: (state, getters) => {
            return state.userInfo.staffCode;
        },
        getUserId: (state, getters) => {
            return state.userInfo.userId;
        },
        getOffice: (state, getters) => {
            const office = localStorage.getItem("officeSelected");
            if (office) {
                state.office = office;
            }
            return state.office;
        },
        getOffices: (state, getters) => {
            return state.offices;
        },
        getSystemConfig: (state, getters) => {
            return state.systemConfig;
        },
    }
})
export default store;