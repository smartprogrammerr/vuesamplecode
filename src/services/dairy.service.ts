﻿import { BaseService } from "./base.service";
import { IUserInfo } from "../models/User";
import { IPaginationResponseModel } from './pagination.response';
import { IDairyModel } from '../models/IDairyModel';


export class DairyService extends BaseService {
    public async getData(
        pageSize: number,
        pageNumber: number,
        keyword: string,
        sortBy: string,
        sortDir: boolean
    ): Promise<IPaginationResponseModel<IDairyModel>> {
        let url =
            "/master/getdata/?pageSize=" +
            pageSize +
            "&pageNumber=" +
            pageNumber;

        if (keyword && keyword != "") {
            url = url + "&keyword=" + keyword;
        }
        if (sortBy && sortBy != "") {
            url = url + "&sortBy=" + sortBy;
        }

        if (sortDir) {
            url = url + "&sortDir=" + sortDir;
        }
        return await this.get<IPaginationResponseModel<IDairyModel>>(url);
    }    
}
