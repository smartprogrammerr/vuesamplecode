﻿import axios, { AxiosInstance, AxiosPromise, AxiosRequestConfig } from 'axios';
import { client } from '../httpclient/axios.client';

export class BaseService {
    public async get<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
        const result = await client.get(url, config);
        return result.data as T;
    }

    public async post<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
        const result = await client.post(url, data, config);
        return result.data as T;
    }

    public async put<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
        const result = await client.put(url, data, config);
        return result.data as T;
    }

    public async patch<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
        const result = await client.patch(url, data, config);
        return result.data as T;
    }

    public async delete<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
        const result = await client.delete(url, config);
        return result.data as T;
    }

    public async head<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
        const result = await client.head(url, config);
        return result.data as T;
    }

    public async options<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
        const result = await client.options(url, config);
        return result.data as T;
    }
}