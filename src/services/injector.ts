﻿import injector from 'vue-inject';
import { UserService as userService } from './user.service';
import { DairyService as dairyService } from './dairy.service';
import { MasterService as masterService } from './master.service';
import { StaffProfileService as staffProfileService } from './staffprofile.service';
import { DueDateService as dueDateService } from './duedate.service';
import { ClientTrackingService as clientTrackingService} from './client.tracking.service';

injector.service('userService', userService);
injector.service('dairyService', dairyService);
injector.service('masterService', masterService);
injector.service('staffProfileService', staffProfileService);
injector.service('dueDateService', dueDateService);
injector.service('clientTrackingService', clientTrackingService);

export class Injector {
    private static user: userService;
    private static dairy: dairyService;
    private static master: masterService;
    private static userProfile: staffProfileService;
    private static duedate: dueDateService;
    private static clientTracking: clientTrackingService;

    public static get UserService(): userService {
        if (!this.user) {
            this.user = injector.get('userService');
        }
        return this.user;
    }

    public static get DairyService(): dairyService {
        if (!this.dairy) {
            this.dairy = injector.get('dairyService');
        }
        return this.dairy;
    }

    public static get MasterService(): masterService {
        if (!this.master) {
            this.master = injector.get('masterService');
        }
        return this.master;
    }

    public static get StaffProfileService(): staffProfileService {
        if (!this.userProfile) {
            this.userProfile = injector.get('staffProfileService');
        }
        return this.userProfile;
    }

    public static get DueDateService(): dueDateService {
        if (!this.duedate) {
            this.duedate = injector.get('dueDateService');
        }
        return this.duedate;
    }

    public static get ClientTrackingService(): clientTrackingService {
        if (!this.clientTracking) {
            this.clientTracking = injector.get('clientTrackingService');
        }
        return this.clientTracking;
    }
}