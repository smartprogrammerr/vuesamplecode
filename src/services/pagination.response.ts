export interface IPaginationResponseModel<T> {
    totalCount: number;   
    items: T[];
  }