﻿import { BaseService } from "./base.service";
import { StaffProfile } from '../models/staffprofile';

export class StaffProfileService extends BaseService {
    public async add(model: StaffProfile): Promise<void> {
        await this.post<void>("/staffprofile/add", model);
    }
    public async getAll(): Promise<StaffProfile[]> {
        return await this.get<StaffProfile[]>('/staffprofile/getall');
    }
    public async getById(id: string): Promise<StaffProfile> {
        return await this.get<StaffProfile>('/staffprofile/getbyid/' + id);
    }
    public async update(model: StaffProfile): Promise<void> {
        await this.put('/staffprofile/update', model);
    }
    public async getByOffice(office: string,clientTrackingId: string | null): Promise<StaffProfile[]> {
        let url = '/staffprofile/pwcoffice/' + office;
        if(clientTrackingId && clientTrackingId != "")
        {
            url += "?clientTrackingId="+clientTrackingId
        }
        return await this.get<StaffProfile[]>(url);
    }

    public async deleteStaffProfile(id: string): Promise<void> {
         await this.delete<void>('/staffprofile/' + id);
    }
}