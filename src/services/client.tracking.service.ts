﻿import { BaseService } from "./base.service";
import {
  ClientTrackingItem,
  DueDateOverride,
  DueDateExtensionOverride,
  RollOverDocumentsResponseModel,
} from "../models/client.tracking.item";
import { BulkUpdateForm } from "../models/bulkupdate";
import { PaginationModel } from "../models/pagedlistmodel";
import { Utils } from "../helper/utils";

export class ClientTrackingService extends BaseService {
  public async add(model: ClientTrackingItem): Promise<void> {
    await this.post<void>("/clienttracking/add", model);
  }
  public async getAll(
    pagination: PaginationModel,
    office: string | null,
    isPersonal: boolean,
    headClientId: string | null
  ): Promise<any> {
    
    if (office) {
      pagination["selectedOffice"] = office;
    }
    else {
      delete  pagination["selectedOffice"]
    }

    if (isPersonal) {
      pagination["isPersonal"] = isPersonal;
    }
    else {
      delete pagination["isPersonal"];
    }

    if (headClientId) {
      pagination["headClientId"] = headClientId;
    }
    else {
      delete pagination["headClientId"]
    }
    const querystring = Utils.objectToQueryString(pagination);
    return await this.get<ClientTrackingItem[]>(
      "/clientTracking/getAll?" + querystring
    );
  }

  public async getAllUnArchived(
    pagination: PaginationModel,
    office: string | null
  ): Promise<any> {
    if (office) {
      pagination["selectedOffice"] = office;
    }
    const querystring = Utils.objectToQueryString(pagination);
    return await this.get<ClientTrackingItem[]>(
      "/clientTracking/getallunarchived?" + querystring
    );
  }

  public async getAllArchived(
    pagination: PaginationModel,
    office: string | null
  ): Promise<any> {
    if (office) {
      pagination["selectedOffice"] = office;
    }
    const querystring = Utils.objectToQueryString(pagination);
    return await this.get<ClientTrackingItem[]>(
      "/clientTracking/getallarchived?" + querystring
    );
  }

  public async getById(id: string): Promise<ClientTrackingItem> {
    return await this.get<ClientTrackingItem>("/clientTracking/getById/" + id);
  }

  public async getRollOverDocumentCount(): Promise<number> {
    return await this.get<number>("/clientTracking/getrolloverdocumentcount");
  }

  public async update(model: ClientTrackingItem): Promise<void> {
    await this.put<void>("/clientTracking/update", model);
  }

  public async archiveSelected(selectedDocumentIds: string[]): Promise<any> {
    await this.post<void>("/clientTracking/archive", selectedDocumentIds);
  }

  public async unArchiveSelected(selectedDocumentIds: string[]): Promise<any> {
    await this.post<void>("/clientTracking/unarchive", selectedDocumentIds);
  }

  public async rollOverDocuments(
    selectedDocumentIds: string[]
  ): Promise<RollOverDocumentsResponseModel> {
    return await this.post<RollOverDocumentsResponseModel>(
      "/clientTracking/rolloverdocuments",
      selectedDocumentIds
    );
  }

  public async bulkUpdateDocuments(model: BulkUpdateForm): Promise<void> {
    await this.put<void>("/clientTracking/bulkUpdate", model);
  }

  public async updateDueDateOverride(model: DueDateOverride): Promise<void> {
    await this.put<void>("/clienttracking/override/duedate", model);
  }

  public async updateDueDateExtensionOverride(
    model: DueDateExtensionOverride
  ): Promise<void> {
    await this.put<void>("/clienttracking/override/extendeduedate", model);
  }

  public async exportSelectedDocuements(
    selectedDocumentIds: string[],
      allFields: boolean, sortBy: string | null, descending: boolean | null,
      exportAll: boolean, office: string | null, search: string | null,
      isPersonal: boolean, headClientId: string | null
  ): Promise<any> {
      let url = "/clientTracking/export?allFields=" + allFields + "&exportAll=" + exportAll;
    if(sortBy && sortBy != null){
      url += "&sortBy=" +sortBy;
    }

    if(descending && descending != null){
      url += "&descending=" +descending;
    }

    if (office) {
        url += "&selectedOffice=" + office;
    }

    if (search) {
        url += "&search=" + search;
      }

    if (isPersonal) {
      url += "&isPersonal=" + isPersonal;
    }

    if (headClientId) {
      url += "&headClientId=" + headClientId;
    }
      
    return await this.post<any>(
      url,
      selectedDocumentIds,
      { headers: { responseType: "blob" } }
    );
  }

  public async deleteClientTrackingItem(id: string): Promise<void> {
    await this.delete<void>("/clienttracking/" + id);
  }

  public async updateClientInfo(id: string): Promise<void> {
    await this.put<void>("/clientTracking/updateclientinfo/"+ id);
  }
}
