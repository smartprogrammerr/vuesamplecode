﻿import { BaseService } from "./base.service";
import { DueDateExtension } from '../models/duedate.extension';

export class DueDateService extends BaseService {
    public async add(model: DueDateExtension): Promise<void> {
        await this.post<void>("/duedateextention/add", model);
    }
    public async getAll(): Promise<DueDateExtension[]> {
        return await this.get<DueDateExtension[]>('/duedateextention/getall');
    }
    public async getById(id: string): Promise<DueDateExtension> {
        return await this.get<DueDateExtension>('/duedateextention/getById/' + id);
    }
    public async getFormNumberByJurisdiction(jurisdiction: string,clientTrackingId: string | null): Promise<any> {
        let url = '/duedateextention/jurisdiction/' + jurisdiction + '/formNumbers';
        if(clientTrackingId && clientTrackingId != "")
        {
            url += "?clientTrackingId="+clientTrackingId
        }
        return await this.get<any>(url);
    }
    public async getByJurisdictionAndFormNumber(jurisdiction: string, formNumber: string): Promise<DueDateExtension> {
        return await this.get<DueDateExtension>('/duedateextention/jurisdiction/' + jurisdiction + '/formNumber/' + formNumber);
    }
    public async update(model: DueDateExtension): Promise<void> {
        await this.put('/duedateextention/update', model);
    }
    public async deleteDueDateExtension(
        id: string
      ): Promise<void> {
        await this.delete<void>("/duedateextention/"+id);
      }
}