﻿import { BaseService } from "./base.service";
import { IUserInfo } from "../models/User";


export class UserService extends BaseService {
    public async getRoles(): Promise<IUserInfo> {
        return await this.get<IUserInfo>("/master/userinfo?t=" + new Date().getTime());
    }

    public async getUsers(
        pageSize: number,
        pageNumber: number,
        includeAvatar: boolean,
        keyword: string | null
    ): Promise<any> {
        let url =
            "organizations/PwCCanada/users?includeAvatar=" +
            includeAvatar +
            "&pageSize=" +
            pageSize +
            "&pageNumber=" +
            pageNumber;

        if (keyword && keyword != "") {
            url = url + "&queryKeyword=" + keyword;
        }
        return await this.get<any>(url);
    }

    public async logout(): Promise<string> {
        return await this.post<string>('/master/logout');
    }
}
