﻿import { BaseService } from "./base.service";
import { SelectItem, SystemConfig } from "../models/master";

export class MasterService extends BaseService {
    public async getIndustries(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/industries");
    }

    public async getJurisdictions(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/jurisdictions");
    }

    public async getJurisdictionShortForms(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/jurisdictionsshort");
    }

    public async getOffices(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/pwcoffices");
    }

    public async getStatusComments(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/statuscomments");
    }

    public async getFormTypes(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/formtypes");
    }

    public async getEntityTypes(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/entityTypes");
    }

    public async getPreprationSoftwares(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/preparationsoftwares");
    }

    public async getInformationRequestStatuses(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/informationrequeststatuss");
    }

    public async getEngagementLetterStatuses(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/engagementletterstatuses");
    }

    public async getStatuses(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/statuses");
    }

    public async getYesNoValues(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/yesno");
    }

    public async getSendNotificatonToValues(): Promise<SelectItem[]> {
        return await this.get<SelectItem[]>("/master/sendnotificationto");
    }

    public async getSystemConfig(): Promise<SystemConfig> {
        return await this.get<SystemConfig>("/master/systemconfig");
    }
}   