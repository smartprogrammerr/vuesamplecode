import Vue from "vue";
import { mapGetters } from 'vuex';
import { Component } from "vue-property-decorator";
import { Notification } from "../../helper/notification";


@Component({
    computed: {
        ...mapGetters({ roles: 'getRoles' }),
        ...mapGetters({ email: 'getEmail' }),
        ...mapGetters({ organizationCode: 'getOrganizationCode' }),
        ...mapGetters({ staffCode: 'getStaffCode' }),
        ...mapGetters({ userId: 'getUserId' }),
    }
})
export class Base extends Vue {
    public roles: string[];
    public email: string;
    public organizationCode: string;
    public staffCode: string;
    public userId: string;
    public notify(status: boolean, message: string) {
        Notification.show(status, message);
    }

    public checkRoleIsGrant(roles: string[] | string): boolean {
        if (typeof (roles) === 'string') {
            const index = this.roles.indexOf(roles);
            return index === -1 ? false : true;
        }
        else {
            let isAccess: boolean = false;
            roles.forEach((role: string) => {
                const index = this.roles.indexOf(role);
                if (index > -1) {
                    isAccess = true;
                    return;
                }
            })
            return isAccess;
        }
        return false;
    }
}
