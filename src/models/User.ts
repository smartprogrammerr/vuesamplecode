﻿export interface IRole {
  name: string;
}

export interface IUserInfo {
  userId: string;
  email: string;
  firstName: string;
  lastName: string;
  organizationCode: string;
  phone: string;
  staffCode: string;
  staffName: string;
  avatar: string;
  roles: string[];
}
