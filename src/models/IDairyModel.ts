export interface IDairyModel {
  id: number,
  name: string;
  category: string;
  dairy: string;
  count: number; 
}