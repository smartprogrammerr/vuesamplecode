﻿import { StaffInfo } from './staff.info';

export class ClientTrackingItem {
    public id: string;
    public parentId: string;
    public client: string;
    public headClient: boolean;
    public headClientDisp: string;
    public clientCode: string;
    public clientContact: string;
    public clientPhone: string;
    public clientEmail: string;
    public comments: string;
    public industriesText: string;
    public industriesValue: string;
    public afsDate: string | null;
    public afsRequired: 'No' | 'Yes';
    public afsRequested: 'No' | 'Yes';
    public dateRequested: string | null;
    public dateRequestedDisp: string | null;
    public afsApproved: 'No' | 'Yes';
    public dateApproved: string | null;
    public dateApprovedDisp: string | null;
    public engagementLetterStatus: string;
    public dateDrafted: string | null;
    public dateDraftedDisp: string | null;
    public dateSent: string | null;
    public dateSentDisp: string | null;
    public dateReceived: string | null;
    public dateReceivedDisp: string | null;
    public engCenter: 'No' | 'Yes';
    public informationRequestStatus: string;
    public pwCContact: string | null;
    public pwCOffice: string;

    public status: string;
    public statusComments: string;
    public statusCommentsValue: string;
    public entityType: string;
    public entityTypeValue: string;
    public preprationSoftware: string;
    public preprationSoftwareValue: string;
    public oneSourceHeadClientName: string;
    public oneSourceCode: string;
    public jurisdiction: string;
    public jurisdictionValue: string;
    public formNumber: string;
    public formNumberValue: string;
    public yearEnd: string | null;
    public yearEndDateDisp: string | null;
    public dueDate: string | null;
    public dueDateDisp: string | null;
    public returnExtended: 'No' | 'Yes';
    public sendNotification: 'No' | 'Yes';
    public notificationRecipsValue: string[];
    public clientExpectedDeadline: string | null;
    public clientExpectedDeadlineDisp: string | null;
    public sendNotificationDeadline: 'No' | 'Yes';
    public reminderList: string[];
    public attachmentText: string;
    public dueDateExtentionPeriodLookUpValues: string | null;
    public pwCContactInfo: StaffInfo | null;

    public dueDateLate: string | null;
    public dueDateLateDisp: string | null;
    public extDateLate: string | null;
    public extDateLateDisp: string | null;

    public dueDateOverriden: boolean | null;
    public extendedDateOverriden: boolean | null;

    public extendedDate: string | null;
    public extendedDateDisp: string | null;
    public notificationDate: string | null;
    public notificationDateDisp: string | null;

    public computedExtendedDate: string | null;
    public computedDueDate: string | null;

    public preparerValue: string | null;
    public preparer2Value: string | null;
    public reviewerValue: string | null;
    public reviewer2Value: string | null;
    public partnerValue: string | null;
    public signerValue: string | null;

    public preparerText: string | null;
    public preparer2Text: string | null;
    public reviewerText: string | null;
    public reviewer2Text: string | null;
    public partnerText: string | null;
    public signerText: string | null;

    public prepEstHours: string | number;
    public prepEstHours2: string | number;
    public revEstHours: string | number;
    public revEstHours2: string | number;
    public signEstHours: string | number;
    public partEstHours: string | number;
    public totalEstHours: number;

    public prepInitials: string;
    public prepInitials2: string;
    public revInitials: string;
    public revInitials2: string;
    public signerInitials: string;
    public partnerInitials: string;

    public txtDueDaysValue: string;
    public txtDueMthsValue: string;
    public txtExtDaysValue: string;
    public txtExtMthsValue: string;

    public formTypeValue: string;

    public signerEmail: string;
    public partnerEmail: string;
    public preparer2Email: string;
    public reviewerEmail: string;
    public reviewer2Email: string;
    public preparerEmail: string;

    public parentClient: string;

    public constructor() {
        this.id = "";
        this.parentId = '';
        this.client = '';
        this.headClient = false;
        this.headClientDisp = "";
        this.clientCode = '';
        this.clientContact = '';
        this.clientPhone = '';
        this.clientEmail = '';
        this.comments = '';
        this.industriesText = '';
        this.industriesValue = '';
        this.afsDate = null;
        this.afsRequired = 'No';
        this.afsRequested = 'No';
        this.dateRequested = null;
        this.dateRequestedDisp = null;
        this.afsApproved = 'No';
        this.dateApproved = null;
        this.dateApprovedDisp = null;
        this.engagementLetterStatus = 'Not Started';
        this.dateDrafted = null;
        this.dateDraftedDisp = null;
        this.dateSent = null;
        this.dateSentDisp = null;
        this.dateReceived = null;
        this.dateReceivedDisp = null;
        this.engCenter = 'No';
        this.yearEndDateDisp = "";
        this.informationRequestStatus = 'Not Started';
        this.pwCContact = '';
        this.pwCOffice = '';

        this.status = 'Pending Info';
        this.statusComments = '';
        this.statusCommentsValue = '';
        this.entityType = '';
        this.entityTypeValue = '';
        this.preprationSoftware = '';
        this.preprationSoftwareValue = '';
        this.oneSourceHeadClientName = '';
        this.oneSourceCode = '';
        this.jurisdiction = '';
        this.jurisdictionValue = '';
        this.formNumber = '';
        this.formNumberValue = '';
        this.yearEnd = null;
        this.dueDate = null;
        this.dueDateDisp = null;
        this.returnExtended = 'No';
        this.extendedDate = null;
        this.extendedDateDisp = null;
        this.notificationDate = null;
        this.notificationDateDisp = null;
        this.sendNotification = 'Yes';
        this.notificationRecipsValue = ['Preparer', 'Reviewer', 'Reviewer2'];
        this.clientExpectedDeadline = '';
        this.clientExpectedDeadlineDisp = '';
        this.sendNotificationDeadline = 'Yes';
        this.reminderList = ['First Preparer', 'Second Preparer', 'First Reviewer', 'Second Reviewer', 'Partner', 'Signer'];
        this.attachmentText = '';
        this.dueDateExtentionPeriodLookUpValues = '';
        this.pwCContactInfo = null;

        this.dueDateLate = null;
        this.dueDateLateDisp = null;
        this.extDateLate = null;
        this.extDateLateDisp = null;

        this.dueDateOverriden = false;
        this.extendedDateOverriden = false;

        this.computedDueDate = null;
        this.computedExtendedDate = null;

        this.preparerValue = '';
        this.preparer2Value = '';
        this.reviewerValue = '';
        this.reviewer2Value = '';
        this.partnerValue = '';
        this.signerValue = '';

        this.preparerText = '';
        this.preparer2Text = '';
        this.reviewerText = '';
        this.reviewer2Text = '';
        this.partnerText = '';
        this.signerText = '';

        this.industriesText = "";
        this.industriesValue = "";
        this.prepEstHours = '';
        this.prepEstHours2 = '';
        this.revEstHours = '';
        this.revEstHours2 = '';
        this.signEstHours = '';
        this.partEstHours = '';
        this.totalEstHours = 0;

        this.prepInitials = '';
        this.prepInitials2 = '';
        this.revInitials = '';
        this.revInitials2 = '';
        this.signerInitials = '';
        this.partnerInitials = '';

        this.txtDueDaysValue = '0';
        this.txtDueMthsValue = '0';
        this.txtExtDaysValue = '0';
        this.txtExtMthsValue = '0';
        this.formTypeValue = "";

        this.signerEmail = "";
        this.partnerEmail = "";
        this.preparerEmail = "";
        this.preparer2Email = "";
        this.reviewerEmail = "";
        this.reviewer2Email = "";

        this.parentClient = '';
    }
}
export enum DateFormat {
    YYYYMMDD,
    DDMMYYYY,
    MMDDYYYY,
    MMMDDYYYY
}

export enum PageMode {
    Edit,
    View,
    New,
    Copy,
    AdditionalReturn,
    AddChild
}
export enum PageListMode {
    ClientDeadLine,
    AllData,
    Personal,
    ClientTrackingChild
}

export const Loader = {
    industry: false,
    pwcOffice: false,
    jurisdiction: false,
    statusComment: false,
    entityType: false,
    preprationSoftware: false,
    status: false,
    engagementLetter: false,
    formNumber: false,
    staffProfile: false
}

export class DueDateOverride {
    public id: string;
    public dueDate: string | null;
    public dueDateLate: string | null;
    public dueDateLateDisp: string | null;
    public dueDateOverriden: boolean | null;

    public constructor() {
        this.id = '';
        this.dueDate = null;
        this.dueDateLate = null;
        this.dueDateLateDisp = null;
        this.dueDateOverriden = false;
    }
}
export class DueDateExtensionOverride {
    public id: string;
    public extendedDate: string | null;
    public extDateLate: string | null;
    public extDateLateDisp: string | null;
    public extendedDateOverriden: boolean | null;

    public constructor() {
        this.id = '';
        this.extendedDate = null;
        this.extDateLate = null;
        this.extDateLateDisp = null;
        this.extendedDateOverriden = false;
    }
}

export class AddDueDateOverride {
    public dueDateLate: string | null;
    public dueDateLateDisp: string | null;
    public constructor() {
        this.dueDateLate = null;
        this.dueDateLateDisp = null;
    }
}
export class AddDueDateExtensionOverride {
    public extDateLate: string | null;
    public extDateLateDisp: string | null;
    public constructor() {
        this.extDateLate = null;
        this.extDateLateDisp = null;
    }
}

export enum Roles {
    Administrator = "Administrator",
    OfficeAdmin = "OfficeAdmin",
    User = "User"
}

export class RollOverDocumentsResponseModel{
    public documentsRolledOver: number;
    public documentsNotRolledOver: number;

    public constructor() {
        this.documentsRolledOver = 0;
        this.documentsNotRolledOver = 0;
    }
}