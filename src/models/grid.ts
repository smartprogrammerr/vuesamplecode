﻿export interface Column  {
    field?: string;
    format?: string;
    filterable?: boolean | string;
    locked?: boolean | string;
    groupable?: boolean | string; 

    filter?: string | ((...args: any[]) => string);
    title?: string | ((...args: any[]) => string);
    width?: string | ((...args: any[]) => string);

    command?: Command;
    cell?: any;
    cellRender?: any;
    template?: Function;
}

interface Command {
    text?: string;
    click?: ((...args: any[]) => void);
}

interface Sorting {
    field: string;
    dir: string;
}

interface Group {
    field: string;
}

export interface GridOptionModel {
    sortable: boolean;
    filterable: boolean;
    groupable: boolean;
    reorderable: boolean;
    expanded: boolean;
    skip: number;
    take: number;
    scrollable: string;
    filter: any;
    sort: Sorting[] | undefined | null;
    group: Group[] | undefined | null;
}

export class GridOption implements GridOptionModel {
    public sortable: boolean;
    public filterable: boolean;
    public groupable: boolean;
    public reorderable: boolean;
    public expanded: boolean;
    public skip: number;
    public take: number;
    public scrollable: 'none' | 'scrollable' | 'virtual';
    public filter: any;
    public sort: Sorting[] | undefined | null;
    public group: Group[] | undefined | null;

    public constructor(sortable: boolean = true,
        filterable: boolean = true,
        groupable: boolean = true,
        reorderable: boolean = true,
        expanded: boolean = false,
        skip: number = 0,
        take: number = 20,
        scrollable: 'none' | 'scrollable' | 'virtual' = 'scrollable',
        filter?: null,
        sort?: Sorting[],
        group?: Group[]
    ) {
        this.sortable = sortable;
        this.filterable = filterable;
        this.groupable = groupable;
        this.reorderable = reorderable;
        this.expanded = expanded;
        this.skip = skip;
        this.take = take;
        this.scrollable = scrollable;
        this.filter = filter;
        this.sort = sort;
        this.group = group;
    }
}

export interface Action {
    type: 'button' | 'link' | 'anchor' | 'txtbutton';
    icon?: string;
    title?: string;
    color?: string;
    visible?: boolean | ((...args: any[]) => boolean);
    disable?: boolean | ((...args: any[]) => boolean);
    action?: (...args: any[]) => void;
    loading?: boolean | ((...args: any[]) => boolean);
}