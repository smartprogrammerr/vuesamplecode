﻿export interface GridHeader  {
    text?: string;
    align?: string;
    filterable?: boolean | string;
    sortable?: boolean | string;
    groupable?: boolean | string;
    value?: string;     
}