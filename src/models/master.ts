﻿export interface SelectItem {
    text: string;
    value: string;
    formattedText?: string;
    formattedTextValue?: string;
}

export interface SystemConfig {
    dmSettingUrl: string;
    dmGlobalSearchUrl: string;
}