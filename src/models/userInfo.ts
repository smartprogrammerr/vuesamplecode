﻿export interface UserInfo {
    avatar: string;
    email: string;
    firstName: string;
    lastName: string;
    organizationCode: string;
    phone: string;
    roles: string[];
    staffCode: string;
    staffName: string;
    userId: string;
}