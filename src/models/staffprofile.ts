﻿import { StaffInfo } from './staff.info';

export class StaffProfile {
    public id: string;
    public staffInfo: StaffInfo | null;
    public pwCOffice: string;
    public staffNameDisp: string;
    public ptin: string;
    public initials: string;
    public email: string;

    constructor() {
        this.id = '';
        this.staffInfo = null;
        this.pwCOffice = '';
        this.ptin = '';
        this.initials = '';
        this.staffNameDisp = '';
        this.email = '';
    }
}