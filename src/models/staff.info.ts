﻿export interface StaffInfo {
    email: string;
    organizationCode: string;
    userId: string;
    staffName: string;
    staffCode: string;
    staffId: string;
    avatar: string;
    formattedAvatar: string | null;
    isSelected: boolean;
}