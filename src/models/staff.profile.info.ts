﻿export interface StaffProfileInfo {
    staffName: string;
    createdBy: string;
    office: string;   
    ptin: string;
    initials: string;
}