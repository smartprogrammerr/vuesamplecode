export interface PageListModel<T> {
    pageNumber?: number;
    pageSize?: number;
    totalRecords?: number;
    totalPages?: number;
    records?: T[];
    total?: number;
    offset?: number;
    items?: T[];
    results?: T[];
}

export class PaginationModel  {
    public sortBy: string;
    public descending: boolean;
    public page: number;
    public rowsPerPage: number;
    public totalItems: number;
    public search: string;

    public constructor( sortBy: string = 'name',
                        descending: boolean = false,
                        rowsPerPage: number = 10,
                        page: number = 1,
                        totalItems: number = 0,
                        search: string = '') {
        this.sortBy = sortBy;
        this.descending = descending;
        this.rowsPerPage = rowsPerPage;
        this.page = page;
        this.totalItems = totalItems;
        this.search = search;
    }
}


export interface Icon {
    class?: string | ((...args: any[]) => string);
    color?: string | ((...args: any[]) => string);
}

export interface Header {
    type?: 'text' | 'icon' | 'checkbox';
    icon?: Icon;
    onChange?: (...args: any[]) => void;
    onClick?: (...args: any[]) => void;
}

export interface VAction {
    type: 'button' | 'link' | 'anchor' | 'txtbutton';
    icon?: string;
    title?: string;
    color?: string;
    visible?: boolean | ((...args: any[]) => boolean);
    disable?: boolean | ((...args: any[]) => boolean);
    action?: (...args: any[]) => void;
    loading?: boolean | ((...args: any[]) => boolean);
}