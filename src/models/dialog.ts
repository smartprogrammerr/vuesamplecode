﻿interface DialogAction {
    text?: string;
    icon?: string;
    color?: string;
    disable?: boolean;
}

export interface DialogOption {
    title?: string;
    content?: string;
    show?: boolean;
    bodyClass?: string;
    
    close?: DialogAction;
    action?: DialogAction;
    width?: string | number;
    contentClass?: string;
    loading?: boolean;

    onClose?: (() => void) | undefined;
    onAction?: (() => void) | undefined;
}