﻿import Vue from 'vue';
const EventBus = new Vue();

export class Snackbar {
    public text: string | undefined;
    public isOpen: boolean | undefined;

    constructor() {
        this.isOpen = false;
        this.text = '';

        EventBus.$on('snackbar', (text: string) => {
            this.text = text;
            this.isOpen = true;
        });
    }

    public static show(text: string): void {
        EventBus.$emit('snackbar', text);
    }
}

export class NavigationDrawer {
    public open: boolean | undefined;
    public static clipped: boolean = true;
    constructor() {
        this.open = true;

        EventBus.$on('drawer_toggle', () => {
            this.open = !this.open;
        });
    }

    public static toggle(): void {
        EventBus.$emit('drawer_toggle');
    }
}

export abstract class ExceptionHandler {
    private static callback: Function | undefined;

    public static init(callback: Function) {
        this.callback = callback;
    }
    public static open(title: string, message: string) {
        if (typeof (this.callback) === 'function') {
            this.callback(title, message);
        }
    }
}