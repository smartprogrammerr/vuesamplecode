﻿export class DueDateExtension {
    public id: string;
    public jurisdiction: string;
    public formNumber: string;
    public originalDueDateMonths: string;
    public originalDueDateDays: string;
    public originalDueDateDisp: string;
    public extensionDueDateMonths: string;
    public extensionDueDateDays: string;

    public extentionPeriodDisp: string;
    public jurisdictionShortForm: string;

    constructor() {
        this.id = '';
        this.jurisdiction = '';
        this.formNumber = '';
        this.originalDueDateMonths = '';
        this.originalDueDateDays = '15';
        this.extensionDueDateMonths = '';
        this.extensionDueDateDays = '15';
        this.jurisdictionShortForm = '';
        this.originalDueDateDisp = "";
        this.extentionPeriodDisp = "";
    }
}

export const Loader = {
    jurisdiction: false,
    jurisdictionShortForm: false
}