const MenuItems = [
    { title: "Home", icon: "home", link: "/", roles: ["Administrator", "OfficeAdmin","User"],isAdmin : null },
    
    {
        title: 'Admin',
        icon: 'mdi-view-dashboard',
        isAdmin: true,
        roles: ['Administrator','OfficeAdmin'],
        items: [
            {
                title: 'Staff Profiles',
                icon: 'group',
                link: `/staffprofile`,
                roles: ['Administrator','OfficeAdmin'],
                isAdmin: true
            },
            {
                title: 'Due Dates and Extensions',
                icon: 'mdi-av-timer',
                link: `/duedate`,
                roles: ['Administrator','OfficeAdmin'],
                isAdmin: true
            },
            {
                title: 'Application Settings',
                icon: 'settings',
                href: '',
                roles: ['Administrator', 'OfficeAdmin'],
                target: '_blank',
                isAdmin: true
            },
        ]
    },
    {
        title: 'Summary View',
        icon: 'mdi-view-module',
        isAdmin: true,
        roles: ["Administrator", "OfficeAdmin","User"],
        items: [
            {
                title: 'Client / Deadline',
                icon: 'mdi-file-document',
                link: `/clienttracking/clientdeadline`,
                roles: ["Administrator", "OfficeAdmin","User"],
                isAdmin: true
            },
            {
                title: 'All Data',
                icon: 'mdi-database',
                link: `/clienttracking/alldata`,
                roles: ["Administrator", "OfficeAdmin","User"],
                isAdmin: true
            },
            {
                title: "Personal View",
                icon: "mdi-account-box",
                link: "/clienttracking/personal",
                roles: ['Administrator', 'OfficeAdmin'],
                cssClass: "bb",
                isAdmin: true,
            },
        ]
    },
    { title: "Archive", icon: "mdi-briefcase-upload", link: "/clienttracking/archive", isAdmin: true,
    roles: ['Administrator','OfficeAdmin'] },
];

export default MenuItems