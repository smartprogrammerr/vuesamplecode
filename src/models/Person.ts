export class Person {
    constructor(      
        public avatar: string ,
        public formattedAvatar: string | null ,
        public email: string ,
        public firstName: string ,
        public lastName: string,
        public organizationCode: string ,
        public phone: string,
        public staffCode: string,
        public staffName: string ,
        public userId: string,
        public isSelected: boolean 
    ) { }
}