﻿export class BulkUpdateForm {
    public documentNumbers: string[];

    public client: string;
    public clientCode: string;
    public clientContact: string;
    public clientPhone: string;
    public clientEmail: string;
    public industry: string;

    public status: string;
    public jurisdiction: string;
    public formNumber: string;
    public notificationRecipsValue: string[];
    public informationRequestStatus: string;
    public statusComment: string;
    public noSendNotification: string;

    public yearEnd: string | null;
    public yearEndDateDisp: string | null;

    public clientExpectedDeadline: string | null;
    public clientExpectedDeadlineDisp: string;

    public txtDueDaysValue: string;
    public txtDueMthsValue: string;
    public txtExtDaysValue: string;
    public txtExtMthsValue: string;

    public preparerValue: string | null;
    public preparer2Value: string | null;
    public reviewerValue: string | null;
    public reviewer2Value: string | null;
    public partnerValue: string | null;
    public signerValue: string | null;

    public preparerText: string | null;
    public preparer2Text: string | null;
    public reviewerText: string | null;
    public reviewer2Text: string | null;
    public partnerText: string | null;
    public signerText: string | null;

    public prepInitials: string;
    public prepInitials2: string;
    public revInitials: string;
    public revInitials2: string;
    public signerInitials: string;
    public partnerInitials: string;

    public signerEmail: string;
    public partnerEmail: string;
    public preparer2Email: string;
    public reviewerEmail: string;
    public reviewer2Email: string;
    public preparerEmail: string;

    public dueDate: string | null;
    public extendedDate: string | null;
    public notificationDate: string | null;

    constructor() {
        this.documentNumbers = [];

        this.client = '';
        this.clientCode = '';
        this.clientContact = '';
        this.clientPhone = '';
        this.clientEmail = '';
        this.industry = '';

        this.status = '';
        this.jurisdiction = '';
        this.formNumber = '';
        this.notificationRecipsValue = [];
        this.informationRequestStatus = '';
        this.statusComment = '';
        this.noSendNotification = 'No Change';

        this.yearEnd = null;
        this.yearEndDateDisp = '';

        this.clientExpectedDeadline = null;
        this.clientExpectedDeadlineDisp = '';

        this.txtDueDaysValue = '0';
        this.txtDueMthsValue = '0';
        this.txtExtDaysValue = '0';
        this.txtExtMthsValue = '0';

        this.preparerValue = '';
        this.preparer2Value = '';
        this.reviewerValue = '';
        this.reviewer2Value = '';
        this.partnerValue = '';
        this.signerValue = '';

        this.preparerText = '';
        this.preparer2Text = '';
        this.reviewerText = '';
        this.reviewer2Text = '';
        this.partnerText = '';
        this.signerText = '';

        this.prepInitials = '';
        this.prepInitials2 = '';
        this.revInitials = '';
        this.revInitials2 = '';
        this.signerInitials = '';
        this.partnerInitials = '';

        this.signerEmail = "";
        this.partnerEmail = "";
        this.preparerEmail = "";
        this.preparer2Email = "";
        this.reviewerEmail = "";
        this.reviewer2Email = "";

        this.dueDate = null;
        this.extendedDate = null;
        this.notificationDate = null;
    }
}

export const Loader = {
    jurisdiction: false,
    statusComment: false,
    status: false,
    formNumber: false,
    staffProfile: false,
    industry: false
}